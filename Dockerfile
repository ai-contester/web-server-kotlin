FROM    openjdk:8-jdk-alpine

COPY    . /src/

RUN apk --update add bash && \
    rm -rf /var/cache/apk/* && \
    cd /src/ && \
    ./gradlew build && \
    mkdir /app && \
    cp /src/build/libs/* /app/ && \
    rm -rf /src && \
    apk del bash

EXPOSE  3000


WORKDIR /app/
CMD     ["java", "-jar", "web-server.jar"]
