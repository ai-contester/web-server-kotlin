# web-server-kotlin
It is implementation of AI-Contester web server in Kotlin using Spring.

## Build
``` shell
gradle build
```

## Run
``` shell
cd build/libs
java -jar web-server-0.1.0.jar --db_password=passwordToMongo --db_user=usernameToMongo
```

## Configuration
### Database password
Required.
``` shell
db_password
```

### Database user name
Optional.
``` shell
db_user
```
This option is "testUser" by default.

### Database host
Optional.
``` shell
db_host
```
Default host is 127.0.0.1

### Database port
Optional.
``` shell
db_port
```
Default database port is 27017.

### Database name
Optional.
``` shell
db_name
```
Default database name is test.

### Compilation server host
Optional.
``` shell
compilation_host
```
Default compilation server host is 127.0.0.1.

### Compilation server port
Optional.
``` shell
compilation_port
```
Default compilation server port is 3200.

### Port of the web-server
Optional.
``` shell
port
```
Default port is 3000.

### Example
``` shell
java -jar web-server-0.1.0.jar --db_password=mySuperPassword --db_user=contesterUser --db_host=127.0.0.1 --db_port=27017 --db_name=test --port=3000
```

### Environment variables
You can use environment variables instead of options:
``` shell
DB_PASSWORD=mySuperPassword DB_USER=contesterUser PORT=1234 java -jar web-server-0.1.0.jar
```

## Development run
``` shell
gradle bootRun
```
