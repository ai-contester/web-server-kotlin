package server

import org.springframework.data.annotation.Id

class User(var fullname: String, var email: String, var username: String) {

    @Id
    lateinit var id: String
    var password: String? = null

    override fun toString():String = String.format(
            "User [id=%s, username'%s', fullname='%s']",
            id, this.username, fullname
    )
}
