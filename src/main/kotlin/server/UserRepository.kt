package server

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query

interface UserRepository:MongoRepository<User, String> {
    fun findOneByUsername(username: String?): User?

    @Query(fields = "{'password': 0}")
    fun findByUsernameStartsWith(username: String?): List<User>

    fun findOneByEmail(email: String): User?
}
