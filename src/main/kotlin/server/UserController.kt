package server

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.security.Principal

@RestController
@RequestMapping("/users")
class UserController {
    @Autowired
    private lateinit var userRepository: UserRepository

    @Autowired
    private lateinit var strategyRepo: StrategyRepository

    @RequestMapping
    fun users(principal: Principal,
              @RequestParam(value="username-starts-with", required=false) usernameStartsWith: String?,
              @RequestParam(value="has-strategy", required=false) hasStrategy: Boolean=false): List<User> {
        val username= principal.name
        this.validateUser(username)

        val users = if (usernameStartsWith != null) userRepository.findByUsernameStartsWith(usernameStartsWith)
                    else userRepository.findAll()

        return if (hasStrategy) users.filter { user -> strategyRepo.findOneByUserAndStatusOrderByDateDesc(user, "accepted") != null }
               else users
    }

    @RequestMapping(value="{userId}")
    fun readUser(@PathVariable userId: String): User {
        return userRepository.findOne(userId)
    }

    @RequestMapping(method=arrayOf(RequestMethod.POST))
    @ResponseStatus(HttpStatus.CREATED)
    fun createUser(@RequestBody user: User): User {
        if (userRepository.findOneByEmail(user.email) != null) {
            throw UserEmailConflictException()
        }
        if (userRepository.findOneByUsername(user.username) != null) {
            throw UserUsernameConflictException()
        }
        userRepository.insert(arrayListOf(user))
        return user
    }

    @ResponseStatus(value = HttpStatus.CONFLICT, reason = "email")
    class UserEmailConflictException: Exception()

    @ResponseStatus(value = HttpStatus.CONFLICT, reason = "username")
    class UserUsernameConflictException: Exception()

    private fun validateUser(username: String?) {
        userRepository.findOneByUsername(username) ?: throw Exception("username $username is not found")
    }
}
