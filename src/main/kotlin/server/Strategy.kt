package server

import org.bson.types.Binary
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import java.util.*

class Strategy(
        @DBRef var user: User,
        var source: String,
        var status: String,
        var date: Date
) {
    @Id
    lateinit var id: String

    var executable: Binary? = null
    var errorMessage: String? = null
}
