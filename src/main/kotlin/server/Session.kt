package server

import org.bson.types.Binary
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import java.util.*

class Session(
        @DBRef var strategies: List<String>,
        var status: String,
        var date: Date,
        var gameStory: String?
) {
    @Id
    lateinit var id: String

}
