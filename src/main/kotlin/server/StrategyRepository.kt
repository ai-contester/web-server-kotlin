package server

import org.springframework.data.mongodb.repository.MongoRepository

interface StrategyRepository: MongoRepository<Strategy, String> {
    fun findByUserId(userId: String): List<Strategy>
    fun findByUser(user: User): List<Strategy>
    fun findOneByUserAndStatusOrderByDateDesc(user: User, status: String): Strategy?
}
