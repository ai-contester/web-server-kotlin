package server

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate
import java.net.URL
import java.security.Principal

@RestController
@RequestMapping("/sessions")
class SessionController {
    @Autowired
    private lateinit var userRepository: UserRepository

    @Autowired
    private lateinit var strategyRepo: StrategyRepository

    @Autowired
    private lateinit var sessionRepo: SessionRepository

    @Value("\${running.service.host}")
    private lateinit var sessionsHost: String

    @Value("\${running.service.port}")
    private  var sessionsPort: Int = 0

    @RequestMapping(method=arrayOf(RequestMethod.POST))
    @ResponseStatus(HttpStatus.CREATED)
    fun createSession(@RequestBody usernames: List<String?>) : SuccessResponse {
        val strategiesIds = usernames.filter {
            username -> username != null && username.isNotEmpty()
        }.map {
            username -> userRepository.findOneByUsername(username) ?: throw UserNotFoundException()
        }.map {
            user -> strategyRepo.findOneByUserAndStatusOrderByDateDesc(user, "accepted") ?: throw StrategyNotFoundException()
        }.map(Strategy::id)

        val rest = RestTemplate()
        val url = URL("http", sessionsHost, sessionsPort, "/sessions")
        val uri = url.toURI()
        try {
            val resp = rest.postForObject(uri, strategiesIds, SessionResponse::class.java)
        } catch(ex: HttpClientErrorException) {
            throw SessionsError()
        }
        return SuccessResponse("Session created")
    }

    @RequestMapping
    fun getSessions():List<Session> {
        val sessions = sessionRepo.findAll()
        return sessions
    }

    class SuccessResponse(message: String)

    fun getUser(principal: Principal): User {
        return userRepository.findOneByUsername(principal.name) ?: throw Exception("User not found")
    }

    class SessionResponse {
        var sessionId: String? = null
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR,
            reason = "Error while communicate with running service")
    class SessionsError: Exception()

    @ResponseStatus(value = HttpStatus.NOT_FOUND,
            reason = "User not found.")
    class UserNotFoundException: Exception()

    @ResponseStatus(value = HttpStatus.NOT_FOUND,
            reason = "Strategy not found.")
    class StrategyNotFoundException: Exception()
}
