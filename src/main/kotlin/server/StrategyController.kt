package server

import org.bson.types.Binary
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate
import java.lang.reflect.Executable
import java.net.URI
import java.net.URL
import java.security.Principal
import java.util.*

@RestController
@RequestMapping("/strategies")
class StrategyController {
    @Autowired
    private lateinit var strategyRepository: StrategyRepository
    @Autowired
    private lateinit var userRepository: UserRepository

    @Value("\${compilation.server.host}")
    private lateinit var compilationHost: String

    @Value("\${compilation.server.port}")
    private  var compilationPort: Int = 0

    @RequestMapping(method=arrayOf(RequestMethod.GET))
    fun getStrategies(principal: Principal): List<Strategy> {
        val user = getUser(principal)
        return strategyRepository.findByUser(user)
    }

    @RequestMapping(method=arrayOf(RequestMethod.POST))
    @ResponseStatus(HttpStatus.CREATED)
    fun createStrategy(principal: Principal, @RequestBody request: CompilationRequest): Strategy {
        val user = getUser(principal)
        val strategy = Strategy(user=user, date=Date(), source=request.source, status="compiling")
        strategyRepository.insert(strategy)
        val rest = RestTemplate()
        val url = URL("http", compilationHost, compilationPort, "/compile")
        val uri = url.toURI()
        try {
            val resp = rest.postForObject(uri, strategy, CompilationResponse::class.java)
        } catch(ex: HttpClientErrorException) {
            throw CompilationError()
        }
        return strategy
    }

    class CompilationRequest(var source: String)

    fun getUser(principal: Principal): User {
        return userRepository.findOneByUsername(principal.name) ?: throw Exception("User not found")
    }

    class CompilationResponse() {
        var message: String? = null
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR,
            reason = "Error while communicate with compilation server")
    class CompilationError: Exception()
}
