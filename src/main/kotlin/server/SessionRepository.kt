package server

import org.springframework.data.mongodb.repository.MongoRepository

interface SessionRepository:MongoRepository<Session, String>