package server

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal

@RestController
class MeController {
    @Autowired
    private lateinit var userRepository: UserRepository

    @RequestMapping(value="/me")
    fun getMe(principal: Principal): User {
        return userRepository.findOneByUsername(principal.name) ?: throw Exception("username ${principal.name} is not found")
    }
}
